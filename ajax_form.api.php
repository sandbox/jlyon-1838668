<?php

/**
 * Implements hook_ajax_form_submit_ajax_alter
 * @param  array  $commands   [description]
 * @param  array  $form       [description]
 * @param  array  $form_state [description]
 * @return NULL
 */
function hook_ajax_form_submit_ajax_alter(&$commands, $form, &$form_state) {
  // Add a drupal AJAX framework command
  $commands[] = ajax_command_restripe('body');
}



/**
 * An example form using the ajax_form framework.
 */
function ajax_form_example_form($form, &$form_state) {=

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Name'),
    '#required' => TRUE,
  );

  $ajax_settings = array(
    //'callback' => 'mymodule_example_callback'  // This should return an array of $commands
    //'error' => t('An example error message.')
    'status' => t('The form was submitted successfully.')
    'text' => l('Go home', '<front>'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Purchase'),
    '#ajax' => ajax_form_ajaxify($form, $ajax_settings),
  );

  return $form;
}

/**
 * Form validate.
 */
function ajax_form_example_form_validate($form, &$form_state) {
  // Add some validation in here
}


/**
 * Form submit.
 */
function healthbook_rewards_purchase_submit($form, &$form_state) {
  // Add whatever submit logic you like in here
}



