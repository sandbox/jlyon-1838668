The Drupal 7 FAPI #ajax callback interface is great, but it doesn't take care of everything out of the box. This module tries to solve this by providing a standard framework to easily make any form (including node forms) AJAXified.

It does this by providing two functions:

<?php
// Set the '#ajax' param on a submit button to this to easily add the necessary functions
ajax_form_ajaxify(&$form, $settings = array())

// A Generic AJAX submit callback.
ajax_form_submit_ajax($form, &$form_state)
?>
An example $settings array for ajax_form_ajaxify could look like:

<?php
  $ajax_settings = array(
    'callback' => 'mymodule_example_callback'  // This should return an array of $commands
    'error' => t('An example error message.')
    'status' => t('The form was submitted successfully.')
    'text' => l('Go home', '<front>'),
  );
?>
A full example of an AJAXified form can be found in ajax_form.api.php.

This module is sponsored by Albatross Digital.
